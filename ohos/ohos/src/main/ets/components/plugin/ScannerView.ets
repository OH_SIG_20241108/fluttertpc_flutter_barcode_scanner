/**
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { camera } from '@kit.CameraKit';
import { display } from '@kit.ArkUI';
import { CameraService } from './CameraService'
import { PermissionsUtil } from './PermissionsUtil';
import { Params, ScanMode, ScannerDialog } from './ScannerUtils';
import { Any, EventChannel, MethodResult } from '@ohos/flutter_ohos';
import { abilityAccessCtrl, Permissions } from '@kit.AbilityKit';
import { detectBarcode } from '@kit.ScanKit';
import { EventSink, StreamHandler } from '@ohos/flutter_ohos/src/main/ets/plugin/common/EventChannel';
import { GlobalContext } from './GlobalContext';

const TAG = '[ScannerView]';
const width: number = px2vp(display.getDefaultDisplaySync().width)
const height: number = px2vp(display.getDefaultDisplaySync().height)

@Component
struct ScannerView {
  @State result: MethodResult | undefined = undefined;
  @State lineColor: string = '';
  @State cancelButtonText: string = 'Cancel';
  @State isShowFlashIcon: boolean = false;
  @State isContinuousScan: boolean = false;
  @State scanMode: ScanMode = ScanMode.other;
  @State userGrant: boolean = false;
  @State surfaceId: string = '';
  @State isOpen: boolean = false;
  @State vtb: number = 53;
  @State vlr: number = 53;
  @State animationOrdinate: number = 0;
  @State isFront: boolean = false;
  @State timer: number | null = null;
  @State scaleValue: number = 0;
  private mXComponentController: XComponentController = new XComponentController();
  private context: Context = getContext(this);
  private dialog: ScannerDialog = new ScannerDialog(this.context);
  private cameraService: CameraService = new CameraService(this.context);
  private imageStreamSink: EventSink | null = null;

  aboutToAppear(): void {
    this.isOpen = false;
    this.isFront = false;
    this.init();
    this.setDisPlay();
    this.context.eventHub.on('page_show', () => {
      this.cameraService.initCamera(this.surfaceId, this.isFront)
    })
    this.context.eventHub.on('page_hide', () => {
      this.cameraService.preFrontRelease();
    })
    this.context.eventHub.on('press_back', () => {
      this.cancel();
    })
  }

  aboutToDisappear(): void {
    this.timer = null;
    this.imageStreamSink = null;
  }

  onPageShow(): void {
  }

  onPageHide(): void {
  }

  // 初始化扫描结果
  resultInit() {
    if (this.isContinuousScan) {
      this.imageStreamSink = GlobalContext.getContext().getObject("imageStreamSink") as EventSink;
      if (this.imageStreamSink) {
        this.cameraService.onImageArrival(this.cameraService.imageReceiver, this.result, true,
          this.imageStreamSink, this.scanMode, null);
      }
    } else {
      this.cameraService.onImageArrival(this.cameraService.imageReceiver, this.result, this.isContinuousScan,
        null, this.scanMode, (r: detectBarcode.DetectResult) => {
          if (r.scanResults && r.scanResults.length) {
            this.result?.success(r.scanResults[0].originalValue);
            setTimeout(() => {
              this.cancel();
            }, 500);
          }
        });
    }
  }

  setDisPlay() {
    this.vtb = (height - 80) / 2 - 100;
    this.vlr = width / 2 - 140;
  }

  async init() {
    const permissions: Array<Permissions> = ['ohos.permission.CAMERA'];
    let grantStatus = await PermissionsUtil.checkAccessToken(permissions[0]);
    if (grantStatus === abilityAccessCtrl.GrantStatus.PERMISSION_GRANTED) {
      this.userGrant = true;
    } else {
      this.requestCameraPermission();
    }
  }

  // 扫一扫初始化
  scan() {
    this.resultInit()
    const cameraManager = camera.getCameraManager(this.context);
    this.cameraService.createCamera(cameraManager);
    this.cameraService.initCamera(this.surfaceId);
  }

  // 申请权限
  async requestCameraPermission() {
    const context = getContext(this);
    let grantStatus = await PermissionsUtil.reqPermissionsFromUser(context);
    let length: number = grantStatus.length;
    for (let i = 0; i < length; i++) {
      if (grantStatus[i] === 0) {
        this.userGrant = true;
      } else {
        this.userGrant = false;
      }
    }
  }

  // 扫一扫动画
  setQRCodeScanAnimation() {
    this.timer = setInterval(() => {
      animateTo({
        duration: 1000, // 动画时间
        tempo: 0.5, // 动画速率
        curve: Curve.EaseInOut,
        delay: 5, // 动画延迟时间
        iterations: -1, // 动画是否重复播放
        playMode: PlayMode.Normal,
      }, () => {
        this.animationOrdinate = 200 // 扫描动画结束Y坐标
      })
    }, 1000)
  }

  async cancel() {
    this.isOpen = false;
    this.cameraService.setTorchMode(this.isOpen);
    await this.release();
    this.dialog!.hide();
    this.imageStreamSink = null;
  }

  async release() {
    await this.cameraService.releaseCamera();
  }

  build() {
    Flex({ justifyContent: FlexAlign.Center, direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      if (this.userGrant) {
        Stack() {
          // 相机视口=========================================================================start
          Column() {
            XComponent({
              id: 'componentId',
              type: XComponentType.SURFACE,
              controller: this.mXComponentController
            })
              .onLoad(() => {
                // 获取XComponent的surfaceId
                const surfaceId = this.mXComponentController.getXComponentSurfaceId();
                // 设置ViewControl相应字段
                this.surfaceId = surfaceId;
                this.setQRCodeScanAnimation();
                this.scan();
              })// 预览流宽、高，默认单位vp，支持px、lpx、vp
              .position({ x: 0, y: 0 })
              .width('100%')
              .height('100%')

          }
          .width('100%')
          .height('100%')

          //  中间动画方格区域==================================================================start
          Column() {
            Column() {
              // 扫一扫的线
              Column()
                .height(1)
                .width(280)
                .border({
                  style: BorderStyle.Solid,
                  color: {
                    left: Color.Transparent,
                    right: Color.Transparent,
                    top: this.lineColor,
                    bottom: Color.Transparent
                  },
                  width: 1
                })
                .position({ x: 0, y: 0 })
                .translate({ x: 0, y: this.animationOrdinate })
            }
            .border({ style: BorderStyle.Solid, color: Color.Transparent, width: 1 })
            .width(280)
            .height(200)
            .opacity(1)
          }
          .height('100%')
          .width('100%')
          .opacity(0.6)
          .border({
            style: BorderStyle.Solid, color: '#666', width: {
              left: this.vlr,
              right: this.vlr,
              top: this.vtb,
              bottom: (this.vtb)
            }
          })
        }
        .height('100%')
        .width('100%')
        .alignContent(Alignment.Center)
        .gesture(PinchGesture({ fingers: 2 })
          .onActionStart((event: GestureEvent) => {
          })
          .onActionUpdate((event: GestureEvent) => {
            if (event) {
              this.scaleValue = event.scale;
            }
          })
          .onActionEnd((event: GestureEvent) => {
            // 获取双指缩放比例，设置变焦比
            this.cameraService.setZoomRatio(this.scaleValue);
          }))

        // 导航栏 ===================================================================================================start
        Flex({ justifyContent: FlexAlign.SpaceBetween, direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
          Column() {
            Image($r('app.media.camera_switch'))
              .width(40).height(35)
          }
          .width('33%')
          .alignItems(HorizontalAlign.Start)
          .onClick(() => {
            this.isFront = !this.isFront;
            this.cameraService.preFrontRelease().then(() => {
              this.cameraService.initCamera(this.surfaceId, this.isFront).then(() => {
                if (this.isOpen) {
                  setTimeout(() => {
                    this.cameraService.setTorchMode(this.isOpen);
                  }, 200)
                }
              })
            })
          })

          if (this.isShowFlashIcon) {
            Column() {
              Image(this.isOpen ? $r('app.media.flash_open') : $r('app.media.flash_close'))
                .width(40)
                .height(35)
            }
            .width('33%')
            .onClick(() => {
              this.isOpen = !this.isOpen;
              this.cameraService.setTorchMode(this.isOpen);
            })
          }
          Column() {
            Text(this.cancelButtonText)
              .width('100%')
              .height('100%')
              .fontColor(Color.White)
              .textAlign(TextAlign.End)
          }
          .width('33%')
          .onClick(() => {
            this.cancel();
          })
        }
        .width('100%')
        .height(80)
        .padding({
          top: 0,
          bottom: 0,
          left: 10,
          right: 10
        })
        .backgroundColor(Color.Black)
      }
    }
    .width('100%')
    .height('100%')
  }
}

@Builder
export function dialogBuilder(params: Params) {
  ScannerView(params)
}