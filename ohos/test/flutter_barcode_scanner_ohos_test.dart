import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_barcode_scanner_ohos/flutter_barcode_scanner_ohos.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  const MethodChannel methodChannel = MethodChannel('flutter_barcode_scanner');

  TestDefaultBinaryMessengerBinding.instance?.defaultBinaryMessenger
      .setMockMethodCallHandler(
    methodChannel,
        (message) => null,
  );

  test('QRScan', () async {
    String barcodeScanRes = '';
    barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        '#ff6666', 'Cancel', true, ScanMode.qr);
    // Assert
    expect(barcodeScanRes, isNotNull);
  });

  test('BarcodeScan', () async {
    String barcodeScanRes = '';
    barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        '#ff6666', 'Cancel', true, ScanMode.barcode);
    // Assert
    expect(barcodeScanRes, isNotNull);
  });

  test('startBarcodeScanStream', () async {
    FlutterBarcodeScanner.getBarcodeStreamReceiver(
        '#ff6666', 'Cancel', true, ScanMode.barcode)!
        .listen((barcode) => {
        // Assert
        expect(barcode, isNotNull)
    });
  });
}
